﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext() { }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source=PromoCodeFactory.db;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Navigation(e => e.Role).AutoInclude();
                entity.HasOne(p => p.Role)
                      .WithMany(p => p.Employees)
                      .HasForeignKey(p => p.RoleId);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasMany<Employee>()
                      .WithOne(e => e.Role);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Id);
                
                entity.HasMany(r => r.PromoCodes)
                      .WithOne(e => e.Customer)
                      .HasForeignKey(e => e.CustomerId)
                      .OnDelete(DeleteBehavior.Cascade);
                
                entity.HasMany(p => p.Preferences)
                      .WithMany(p => p.Customers)
                      .UsingEntity<CustomerPreference>(
                            cp => cp
                                .HasOne(e => e.Preference)
                                .WithMany()
                                .HasForeignKey(pt => pt.PreferenceId),
                            cp => cp
                                .HasOne(pt => pt.Customer)
                                .WithMany()
                                .HasForeignKey(pt => pt.CustomerId),
                            cp =>
                            {
                                cp.HasKey(t => new { t.CustomerId, t.PreferenceId });
                            });

                entity.Navigation(e => e.Preferences).AutoInclude();
                entity.Navigation(e => e.PromoCodes).AutoInclude();
            });

            modelBuilder.Entity<Preference>().HasKey(e => e.Id);

            modelBuilder.Entity<PromoCode>(entity =>
            {
                entity.HasKey(e => e.Id);
                
                entity.Navigation(e => e.Preference).AutoInclude();
                entity.Navigation(e => e.PartnerManager).AutoInclude();

                entity.HasOne(p => p.Preference)
                      .WithMany();
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
