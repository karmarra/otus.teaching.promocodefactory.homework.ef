﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        private static readonly Guid employeeId = Guid.NewGuid();
        private static readonly Guid employeeId2 = Guid.NewGuid();

        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = employeeId,
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = roleId,
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = employeeId2,
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = role2,
                AppliedPromocodesCount = 10
            },
        };

        private static readonly Guid roleId = Guid.NewGuid();
        private static readonly Guid role2 = Guid.NewGuid();

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = roleId,
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = role2,
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        private static readonly Guid preferenceId = Guid.NewGuid();
        private static readonly Guid preferenceId2 = Guid.NewGuid();
        private static readonly Guid preferenceId3 = Guid.NewGuid();

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = preferenceId,
                Name = "Театр",
            },
            new Preference()
            {
                Id = preferenceId2,
                Name = "Семья",
            },
            new Preference()
            {
                Id = preferenceId3,
                Name = "Дети",
            }
        };

        private static readonly Guid customerId = Guid.NewGuid();
        private static readonly Guid customerId2 = Guid.NewGuid();
        private static readonly Guid customerId3 = Guid.NewGuid();

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_petrov@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров"
                    },
                    new Customer()
                    {
                        Id = customerId2,
                        Email = "ivan_ivanov@mail.ru",
                        FirstName = "Иван",
                        LastName = "Иванов"
                    },
                    new Customer()
                    {
                        Id = customerId3,
                        Email = "ivan_sidorov@mail.ru",
                        FirstName = "Иван",
                        LastName = "Сидоров"
                    }
                };

                return customers;
            }
        }

        public static IEnumerable<CustomerPreference> CustomerPreferences
    => new List<CustomerPreference>
    {
                new CustomerPreference()
                {
                    Id = Guid.NewGuid(),
                    CustomerId = customerId,
                    PreferenceId = preferenceId
                },
                new CustomerPreference()
                {
                    Id = Guid.NewGuid(),
                    CustomerId = customerId,
                    PreferenceId = preferenceId2
                },
                new CustomerPreference()
                {
                    Id = Guid.NewGuid(),
                    CustomerId = customerId3,
                    PreferenceId = preferenceId2
                },
    };

        public static IEnumerable<PromoCode> PromoCodes
        {
            get
            {
                var promoCodes = new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = Guid.NewGuid(),
                        Code = "50%",
                        ServiceInfo = "Скидка новым клиентам",
                        BeginDate = new DateTime(2022, 1, 1),
                        EndDate = new DateTime(2023, 1, 1),
                        PartnerName = "ООО Рога и копыта",
                        PartnerManagerId =  Employees.First().Id,
                        PreferenceId = preferenceId3,
                        CustomerId = customerId3
                    },
                    new PromoCode()
                    {
                        Id = Guid.NewGuid(),
                        Code = "80%",
                        ServiceInfo = "Скидка старым клиентам",
                        BeginDate = new DateTime(2022, 1, 1),
                        EndDate = new DateTime(2023, 1, 1),
                        PartnerName = "ООО Рога и копыта",
                        PartnerManagerId =  Employees.Last().Id,
                        PreferenceId = preferenceId,
                        CustomerId = customerId
                    }
                };
                return promoCodes;
            }
        }
    }
}