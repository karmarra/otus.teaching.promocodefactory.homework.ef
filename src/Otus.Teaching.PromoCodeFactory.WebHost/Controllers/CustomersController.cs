﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse(x)).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получение клиента по ID
        /// </summary>
        /// <param name="id">ID клиента</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var response = new CustomerResponse(customer);
            
            return Ok(response);
        }

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        /// <param name="request">Параметры нового клиента</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            if (request.PreferenceIds != null)
            {
                var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
                customer.Preferences = preferences.ToList();
            }

            await _customerRepository.AddAsync(customer);
            return Ok();
        }

        /// <summary>
        /// Измененить клиента
        /// </summary>
        /// <param name="id">ID клиента</param>
        /// <param name="request">Необходимые поля для изменения</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            if (request.PreferenceIds != null)
            {
                var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
                customer.Preferences = preferences.ToList();
            }

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id">ID клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            while (customer.PromoCodes?.Count() > 0)
            {
                var promoCode = customer.PromoCodes.First();
                await _promoCodeRepository.DeleteAsync(promoCode.Id);
                customer.PromoCodes.Remove(promoCode);
            }
            await _customerRepository.DeleteAsync(id);

            return Ok();
        }
    }
}