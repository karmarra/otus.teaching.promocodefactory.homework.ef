﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public PromocodesController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }


        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var response = promoCodes.Select(x => new PromoCodeShortResponse(x)).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferenceRepository.GetByIdAsync(request.PreferenceId);

            if (preference == null)
                return NotFound();

            var customers = await _customerRepository.GetAllAsync();
            var customerWithPreference = customers
                .Where(c => c.Preferences.Any(p => p.Id == preference.Id));
            
            foreach (var customer in customerWithPreference)
            {
                var promoCode = new PromoCode
                {
                    Id = Guid.NewGuid(),
                    ServiceInfo = request.ServiceInfo,
                    Preference = preference,
                    BeginDate = DateTime.Now,
                    Code = request.PromoCode,
                    EndDate = DateTime.Now.AddMonths(1),
                    PartnerName = request.PartnerName,
                    Customer = customer,
                };

                await _promoCodeRepository.AddAsync(promoCode);

                customer.PromoCodes.Add(promoCode);
                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}