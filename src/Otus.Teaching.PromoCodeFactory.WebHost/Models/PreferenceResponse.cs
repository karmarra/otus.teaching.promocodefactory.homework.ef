﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public PreferenceResponse(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }

        public PreferenceResponse(CustomerPreference preference)
        {
            Id = preference.PreferenceId;
            Name = preference.Preference.Name;
        }

        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}