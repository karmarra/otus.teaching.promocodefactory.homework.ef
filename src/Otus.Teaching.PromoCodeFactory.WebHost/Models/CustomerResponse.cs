﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public CustomerResponse()
        {

        }

        public CustomerResponse(Customer customer)
        {
            var preferences = customer.Preferences?
                .Select(p => new PreferenceResponse(p)
                {
                    Id = p.Id,
                    Name = p.Name
                })
                .ToList();

            var promoCodes = customer.PromoCodes?
                .Select(c => new PromoCodeShortResponse(c))
                .ToList();

            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            Preferences = preferences;
            PromoCodes = promoCodes;
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
        public List<PromoCodeShortResponse> PromoCodes { get; set; }
    }
}